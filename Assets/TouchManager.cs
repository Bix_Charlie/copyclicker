﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TouchManager : MonoBehaviour, IPointerDownHandler
{
    public UnityEvent onTouch;

    public void OnPointerDown(PointerEventData eventData)
    {
        onTouch?.Invoke();
        CopiesManager.instance.Click(eventData.position);
    }
}
