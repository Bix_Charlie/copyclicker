﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class StoreItem : MonoBehaviour
{
    public Item storeItem;
    public ulong currentCost;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI name, cost, level, costPerSecond;

    private Button thisButton;

    private void Start()
    {
        thisButton = GetComponent<Button>();
    }

    private void LateUpdate()
    {
        ButtonEnabler(CopiesManager.instance.copies >= currentCost);
    }

    private void ButtonEnabler(bool active)
    {
        thisButton.interactable = active;
    }

    public void GenerateMaker()
    {
        CopiesManager.instance.AddCopiesperSecond(storeItem.cps);
        CopiesManager.instance.SubtractCopies(currentCost);
        currentCost = (ulong)(currentCost * 1.15f);
        storeItem.level++;
        cost.SetText(currentCost.ToString());
        level.SetText($"Level: {storeItem.level}");
    }
    public void Init(Helper helper)
    {
        storeItem = helper.storeItem;
        image.sprite = storeItem.icon;
        name.SetText(storeItem.name);
        cost.SetText(storeItem.cost.ToString());        
        level.SetText($"Level: {storeItem.level}");
        costPerSecond.SetText($"CPS: {storeItem.cps}");
    }

    public void DisplayInfo()
    {
        name.SetText(storeItem.name);
        level.SetText($"Level: {storeItem.level}");
        CopiesManager.instance.AddCopiesperSecond(storeItem.cps * (ulong)storeItem.level);
        currentCost= (ulong)(storeItem.cost * Math.Pow(1.15f, storeItem.level));
        cost.SetText(currentCost.ToString());
    }
}
