﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class StoreLoader : MonoBehaviour
{
    [SerializeField] private GameObject StoreObject;
    [SerializeField] private GameObject parentObject;
    public List<StoreItem> storeItems;

    Dictionary<string, Item> savedInfo = new Dictionary<string, Item>();
    string path;
    private bool gameHasBegun = false;

    private void Awake()
    {
        path = Application.persistentDataPath + "/items.json";
    }

    private void Start()
    {
        if (File.Exists(path))
        {
            LoadItems();
            gameHasBegun = true;
            print($"Loaded from {path}");
        }
        else
        {
            SaveItems();
        }

        InitializeValues();
        parentObject.SetActive(false);
    }

    private void InitializeValues()
    {
        StoreItem[] storeInfoSetters = storeItems.ToArray();
        Queue itemsLoaded = new Queue();
        foreach (Item item in savedInfo.Values)
        {
            itemsLoaded.Enqueue(item);
        }


        foreach (StoreItem item in storeInfoSetters)
        {
            item.storeItem = (Item)itemsLoaded.Dequeue();
            item.DisplayInfo();
        }
    }


    [ContextMenu("SaveObjects")]
    public void SaveItems()
    {
        path = Application.persistentDataPath + "/items.json";
        File.Create(path).Close();

        savedInfo = new Dictionary<string, Item>();
        storeItems
            .ForEach(x => savedInfo.Add(x.storeItem.name, x.storeItem));

        ItemCollection itemCollection = new ItemCollection() { itemsInfo = savedInfo.Values.ToList() };

        string jsonString = JsonUtility.ToJson(itemCollection);
        File.WriteAllText(path, jsonString);
        print($"SUCCESSFULLY SAVED AT LOCATION {path}");
        print(jsonString);

    }

    [ContextMenu("Initialize Store Assets ScriptableObjects")]
    public void UpdateStoreFromFiles()
    {
        foreach (var item in HelpersFromResource())
        {
            GameObject storeItem = Instantiate(StoreObject, transform);
            storeItem.GetComponent<StoreItem>().Init((Helper)item);
        }
    }

    private Object[] HelpersFromResource()
    {
        return Resources.LoadAll("Helpers", typeof(Helper));
    }

    [ContextMenu("Print Values")]
    public void CheckDictionary()
    {
        foreach (var item in savedInfo)
        {
            print($"{item.Key}:  {item.Value}");
        }
    }

    [ContextMenu("LoadObjects")]
    public void LoadItems()
    {
        string json = File.ReadAllText(path);
        ItemCollection tempCollection = JsonUtility.FromJson<ItemCollection>(json);
        savedInfo = new Dictionary<string, Item>();
        foreach (Item item in tempCollection.itemsInfo)
        {
            savedInfo.Add(item.name, item);
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        print("FOCUS");
        if (gameHasBegun)
        {
            SaveItems();
        }
    }
    private void OnApplicationQuit()
    {
        print("QUIT");
        SaveItems();
    }
}

[System.Serializable]
public struct ItemCollection
{
    public List<Item> itemsInfo;
}

[System.Serializable]
public struct Item
{
    public string name;
    public Sprite icon;
    public ulong cost;
    public int level;
    public ulong cps;
    public override string ToString()
    {
        return $"{name}     cost: {cost}    level:{level} ";
    }
}