﻿using System.IO;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveData(PlayerData data)
    {
        string path = Application.persistentDataPath + "/player.json";
        File.Create(path).Close();
        string jsonString = JsonUtility.ToJson(data);
        File.WriteAllText(path, jsonString);
    }

    public static ulong LoadData()
    {
        string path = Application.persistentDataPath + "/player.json";
        string json = File.ReadAllText(path);
        PlayerData playerData = JsonUtility.FromJson<PlayerData>(json);
        return playerData.copies;
    }
}

public struct PlayerData
{
    public ulong copies;

    public PlayerData(ulong copies)
    {
        this.copies = copies;
    }
}
