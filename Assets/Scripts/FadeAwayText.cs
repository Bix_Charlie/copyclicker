﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FadeAwayText : MonoBehaviour
{
    private RectTransform thisTransform;
    public float fadeDuration;
    // Start is called before the first frame update
    void Start()
    {
        thisTransform = GetComponent<RectTransform>();
        thisTransform.DOAnchorPosY(thisTransform.anchoredPosition.y + 20f, fadeDuration);
        Text thisTExt = GetComponent<Text>();
        Color endColor = thisTExt.color;
        endColor.a = 0f;
        thisTExt.DOColor(endColor, fadeDuration).OnComplete(()=>Destroy(this.gameObject));
    }
}
