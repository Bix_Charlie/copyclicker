﻿namespace Assets.Scripts
{
    public interface ICopyMaker
    {
        int CopyAmount { get; set; }

        void Copy();
    }
}
