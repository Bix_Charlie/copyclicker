﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodinezManager : MonoBehaviour
{
    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void ClickedRecently()
    {
        anim.Play("godinez typing");
    }
}
