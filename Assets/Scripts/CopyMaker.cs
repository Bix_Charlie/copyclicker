﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyMaker : MonoBehaviour
{
    public GameObject[] workers;
    private int index;

    public int Index { get => index; set => index = value; }

    /*
    public GameObject prefab;

    public GameObject AddCopier()
    {
        GetComponent<AutomaticClicker>().amountofClickers++;
        return Instantiate(prefab, this.transform);
    }
    */

    public void AddCopier()
    {
        workers[Index].SetActive(true);
        Index++;
        Index %= workers.Length;
    }
    private void Start() {
        /*
        int cantidad = 0;
        Data data = SaveSystem.LoadData(prefab.name);
        if (data!=null)
        {
            cantidad = data.copierAmounts;
        }

        for (var i = 0; i < cantidad; i++)
        {
            AddCopier();
        }
        */

    }

    private void OnApplicationQuit() {
        //SaveSystem.SaveData(new Data(GetComponent<AutomaticClicker>().amountofClickers, prefab.name));
    }
}
