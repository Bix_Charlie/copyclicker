﻿public interface ITweenShowHide
{
    bool IsOpen { get;}

    void Hide();
    void Show();
    void ToggleShowHide();
}