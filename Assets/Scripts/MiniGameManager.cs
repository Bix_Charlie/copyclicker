﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniGameManager: MonoBehaviour
{
    [SerializeField]
    public static void LoadMiniGame(string gameToLoad)
    {
        if (SceneManager.sceneCount<=1)
        {
            SceneManager.LoadScene(gameToLoad,LoadSceneMode.Additive);            
        }
    }

    public static void UnLoadMiniGame()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name.ToLower().Contains("game") && scene.isLoaded)
            {
                SceneManager.UnloadSceneAsync(scene);
            }
        }
    }
}
