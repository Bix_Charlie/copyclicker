﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Helper", menuName = "Helper")]
public class Helper : ScriptableObject
{
    public Item storeItem;
}
