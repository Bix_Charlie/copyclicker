﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;


[RequireComponent(typeof(Click))]
public class AutomaticClicker : MonoBehaviour
{
    private Click clicker;
    public int copyAmount;
    public int amountofClickers;
    public float timeforClick;

    // Start is called before the first frame update
    void Start()
    {
        clicker = GetComponent<Click>();
    }
}
