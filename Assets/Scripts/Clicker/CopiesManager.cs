﻿using Coffee.UIExtensions;
using System;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;


public class CopiesManager : MonoBehaviour
{
    private static CopiesManager current;

    public ulong copies = 0;
    public ulong copiesPerSecond = 0;
    public ulong copiesClick;
    public TextMeshProUGUI copiesDisplay, copiesPerSecondTxt, copiesPerClick;
    public byte multiplier = 1;
    private Queue<GameObject> particleQueue;
    public int size;
    public GameObject particlePrefab;
    public Canvas canvas;
    private const string DATEQUIT = "dateQuit";
    private string path;

    private AudioSource clickerSound;

    public static CopiesManager instance { get => current; }

    private void Awake()
    {
        path = Application.persistentDataPath + "/player.json";
        current = this;
        clickerSound = GetComponent<AudioSource>();
        if (File.Exists(path))
        {
            copies = LoadData();
            print($"Loaded from {path}");
        }
        else
        {
            SaveData(new PlayerData(copies));
        }
    }


    private void Start()
    {
        ulong copiesSinceLastSession = copiesPerSecond * (ulong)GetSecondsFromLastSession();
        print(copiesSinceLastSession + " were added");
        copies += copiesSinceLastSession;
        UpdateText();
        InvokeRepeating("Loop", 0, 0.1f);
        GenerateQueue();

    }

    public void AddCopiesperSecond(ulong amount)
    {
        copiesPerSecond += amount;
    }

    private void GenerateQueue()
    {
        particleQueue = new Queue<GameObject>();
        for (int i = 0; i < size; i++)
        {
            GameObject obj = Instantiate(particlePrefab, canvas.transform);
            obj.SetActive(false);
            particleQueue.Enqueue(obj);
        }
    }

    private void SpawnFromPool(Vector2 position)
    {
        GameObject objectToSpawn = particleQueue.Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.GetComponent<RectTransform>().position = position;
        objectToSpawn.GetComponent<UIParticle>().Play();
        particleQueue.Enqueue(objectToSpawn);

    }

    private void LateUpdate()
    {
        copiesPerClick.SetText(copiesClick.ToString());
    }

    public void Click(Vector2 clickPosition)
    {
        SpawnFromPool(clickPosition);
        clickerSound.Play();
    }

    private void Loop()
    {
        ulong tickAmount = (ulong)(copiesPerSecond * 0.1f);
        copies += tickAmount;
        UpdateText();
    }



    private int GetSecondsFromLastSession()
    {
        string dateQuitString = PlayerPrefs.GetString(DATEQUIT, "");
        if (!dateQuitString.Equals(""))
        {
            DateTime dateQuit = DateTime.Parse(dateQuitString);
            DateTime dateNow = DateTime.Now;

            if (dateNow > dateQuit)
            {
                TimeSpan timespan = dateNow - dateQuit;
                return timespan.Seconds;
            }
        }
        return 0;
    }


    private void UpdateText()
    {
        string display = string.Format("{0} copies", copies.ToString());
        string copiesPerSec = string.Format("{0} per sec", copiesPerSecond.ToString());
        copiesDisplay.SetText(display);
        copiesPerSecondTxt.SetText(copiesPerSec);
    }

    public void Copy(uint amount)
    {
        copies += amount * multiplier;
        UpdateText();
    }

    public void SubtractCopies(ulong amount)
    {
        copies -= amount;
        UpdateText();
    }


    public void SaveData(PlayerData data)
    {
        File.Create(path).Close();
        string jsonString = JsonUtility.ToJson(data);
        File.WriteAllText(path, jsonString);
    }


    public ulong LoadData()
    {
        string json = File.ReadAllText(path);
        PlayerData playerData = JsonUtility.FromJson<PlayerData>(json);
        return playerData.copies;
    }

    public void ResetToZero()
    {
        copies = 0;
        SaveSystem.SaveData(new PlayerData());
    }

    private void OnApplicationFocus(bool focus)
    {
        SaveToFile();
    }

    private void OnApplicationQuit()
    {
        SaveToFile();
    }

    private void OnApplicationPause(bool pause)
    {
        SaveToFile();
    }

    private void SaveToFile()
    {
        DateTime datequit = DateTime.Now;
        PlayerPrefs.SetString(DATEQUIT, datequit.ToString());
        SaveSystem.SaveData(new PlayerData(copies));
    }
}
