﻿using Assets.Scripts;
using UnityEngine;

public class Click : MonoBehaviour
{

    private uint copyAmount = 1;

    public void Copy()
    {
        CopiesManager.instance.Copy(copyAmount);
    }
}
