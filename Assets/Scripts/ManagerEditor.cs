﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(CopiesManager))]
public class ManagerEditor : Editor
{
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if(GUILayout.Button("Reset to zero"))
        {
            CopiesManager.instance.ResetToZero();
        }
    }
}

#endif
