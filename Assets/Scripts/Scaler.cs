﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Scaler : MonoBehaviour
{
    [SerializeField] [Range(0,1)] private float time = 0.5f;
    void Start()
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one,time);
    }
}

