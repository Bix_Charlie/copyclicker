﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanelSlider : MonoBehaviour, ITweenShowHide
{
    [SerializeField] private float time = 1f;
    [SerializeField] private Vector2 finalPosition;
    private Vector2 initialPosition = Vector2.zero;
    [SerializeField] private Ease Ease = Ease.InOutSine;


    private RectTransform panel;
    private bool isOpen = false;
    public bool IsOpen { get => isOpen; }

    private void Start()
    {
        panel = GetComponent<RectTransform>();
    }

    public void ToggleShowHide()
    {
        if (!isOpen) Show();
        else Hide();
    }
    private void Move(Vector2 position)
    {
        panel.DOAnchorPos(position, time)
            .SetEase(Ease);
    }
    public void Hide()
    {
        Move(initialPosition);
        isOpen = false;
    }
    public void Show()
    {
        Move(finalPosition);
        isOpen = true;
    }
}